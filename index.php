<?php

require_once 'animal.php';
require_once 'ape.php';
require_once 'frog.php';

$sheep = new Animal("shaun");

//Release 0
echo "Nama hewan : ".$sheep->name. "<br>"; // "shaun"
echo "Jumlah kaki : ".$sheep->legs. "<br>"; // 2
echo "Berdarah dingin : ";
echo var_dump($sheep->cold_blooded). "<br><br>"; // false

//Release 1
$sungokong = new Ape("kera sakti");
echo "Nama hewan : ".$sungokong->name. "<br>"; 
echo "Jumlah kaki : ".$sungokong->legs. "<br>"; 
echo "Berdarah dingin : ";
echo var_dump($sungokong->cold_blooded). "<br>";

$sungokong->yell() ;// "Auooo"
echo "<br><br>";


$kodok = new Frog("buduk");
echo "Nama hewan : ".$kodok->name. "<br>"; 
echo "Jumlah kaki : ".$kodok->legs. "<br>"; 
echo "Berdarah dingin : ";
echo var_dump($kodok->cold_blooded). "<br>";

$kodok->jump() ; // "hop hop"

?>